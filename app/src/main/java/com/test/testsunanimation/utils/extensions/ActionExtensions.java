package com.test.testsunanimation.utils.extensions;

import com.test.testsunanimation.interfaces.Action;

public final class ActionExtensions {

    private ActionExtensions() {

    }

    public static void callActionIfNotNull(Action action) {
        if (action != null) {
            action.callAction();
        }
    }
}
