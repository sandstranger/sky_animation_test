package com.test.testsunanimation.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.view.animation.AccelerateInterpolator

import com.test.testsunanimation.colors.SkyColors
import com.test.testsunanimation.ui.view.AnimationFragmentViewHolder

class AnimationFragmentAnimationsUtils private constructor(private val viewHolder: AnimationFragmentViewHolder) {

    private var animationPlaying = false
    private val skyColors: SkyColors

    init {
        skyColors = SkyColors.newInstance(viewHolder.context)
    }

    fun playAnimation() {
        if (animationPlaying) {
            return
        }
        animationPlaying = true
        val animatorSet = buildAnimatorSet()
        animatorSet.start()
    }

    private fun buildSunsetSkyAnimator(): ObjectAnimator {
        val animator = ObjectAnimator.ofInt(viewHolder.skyView,
                "backgroundColor", skyColors.blueSkyColor, skyColors.sunsetSkyColor)
                .setDuration(ANIMATION_DURATION_MILLIS.toLong())
        animator.setEvaluator(ArgbEvaluator())
        return animator
    }

    private fun buildSunHeightAnimator(): ObjectAnimator {
        val sunYStart = viewHolder.sunView.top
        val sunYEnd = viewHolder.skyView.height

        val sunHeightAnimator = ObjectAnimator.ofFloat(viewHolder.sunView, "y", sunYStart, sunYEnd)
                .setDuration(ANIMATION_DURATION_MILLIS.toLong())
        sunHeightAnimator.setInterpolator(AccelerateInterpolator())
        return sunHeightAnimator
    }

    private fun buildNightSkyAnimator(): ObjectAnimator {
        val animator = ObjectAnimator.ofInt(viewHolder.skyView,
                "backgroundColor", skyColors.sunsetSkyColor, skyColors.nightSkyColor)
                .setDuration(NIGHT_SKY_ANIMATION_DURATION_MILLIS.toLong())
        animator.setEvaluator(ArgbEvaluator())
        return animator
    }

    private fun buildAnimatorSet(): AnimatorSet {
        val animatorSet = AnimatorSet()
        animatorSet.play(buildSunHeightAnimator())
                .with(buildSunsetSkyAnimator())
                .before(buildNightSkyAnimator())

        animatorSet.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                animationPlaying = false
            }
        })

        return animatorSet
    }

    companion object {
        private val ANIMATION_DURATION_MILLIS: Int
        private val NIGHT_SKY_ANIMATION_DURATION_MILLIS: Int

        init {
            ANIMATION_DURATION_MILLIS = 3000
            NIGHT_SKY_ANIMATION_DURATION_MILLIS = ANIMATION_DURATION_MILLIS / 2
        }


        fun newInstance(viewHolder: AnimationFragmentViewHolder): AnimationFragmentAnimationsUtils {
            return AnimationFragmentAnimationsUtils(viewHolder)
        }
    }
}
