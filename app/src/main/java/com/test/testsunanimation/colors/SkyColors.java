package com.test.testsunanimation.colors;

import android.content.Context;

import com.test.testsunanimation.R;

import lombok.Getter;
import lombok.val;

public class SkyColors {
    private final int blueSkyColor;
    private final int sunsetSkyColor;
    private final int nightSkyColor;

    private SkyColors(Context context) {
        val resources = context.getResources();
        blueSkyColor = resources.getColor(R.color.blue_sky_color);
        sunsetSkyColor = resources.getColor(R.color.sunset_sky_color);
        nightSkyColor = resources.getColor(R.color.night_sky_color);
    }

    public static SkyColors newInstance(Context context) {
        return new SkyColors(context);
    }

    public int getBlueSkyColor() {
        return blueSkyColor;
    }

    public int getSunsetSkyColor() {
        return sunsetSkyColor;
    }

    public int getNightSkyColor() {
        return nightSkyColor;
    }
}
