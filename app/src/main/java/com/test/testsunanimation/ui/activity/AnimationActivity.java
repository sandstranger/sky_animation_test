package com.test.testsunanimation.ui.activity;

import android.support.v4.app.Fragment;

import com.androidutils.ui.activity.SingleActivity;
import com.test.testsunanimation.ui.fragment.AnimationFragment;

public class AnimationActivity extends SingleActivity {

    @Override
    protected Fragment createFragment() {
        return AnimationFragment.newInstance();
    }
}
