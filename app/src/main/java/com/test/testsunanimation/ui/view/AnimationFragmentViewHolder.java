package com.test.testsunanimation.ui.view;

import android.content.Context;
import android.view.View;

import com.test.testsunanimation.R;
import com.test.testsunanimation.interfaces.Action;
import com.test.testsunanimation.utils.extensions.ActionExtensions;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.ExtensionMethod;

@ExtensionMethod(ActionExtensions.class)
public class AnimationFragmentViewHolder {
    private final View rootView;

    @BindView(R.id.sun_image_view)
    protected View sunView;
    @BindView(R.id.sky_layout)
    protected View skyView;
    @Setter
    private Action rootViewClickedListener;

    private AnimationFragmentViewHolder(View rootView) {
        this.rootView = rootView;
        ButterKnife.bind(this,rootView);
        rootView.setOnClickListener(v -> { rootViewClickedListener.callActionIfNotNull(); });
    }

    public Context getContext(){
        return rootView.getContext();
    }

    public static AnimationFragmentViewHolder newInstance(View rootView) {
        return new AnimationFragmentViewHolder(rootView);
    }

    public View getSunView() {
        return sunView;
    }

    public View getSkyView() {
        return skyView;
    }
}
