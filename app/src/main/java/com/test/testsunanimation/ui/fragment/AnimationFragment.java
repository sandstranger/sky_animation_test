package com.test.testsunanimation.ui.fragment;

import android.support.v4.app.Fragment;
import android.view.View;

import com.androidutils.ui.fragment.SingleFragment;
import com.test.testsunanimation.R;
import com.test.testsunanimation.ui.view.AnimationFragmentViewHolder;
import com.test.testsunanimation.utils.AnimationFragmentAnimationsUtils;

import lombok.val;

public class AnimationFragment extends SingleFragment {
    private AnimationFragmentViewHolder viewHolder;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_animation;
    }

    @Override
    protected void bindView(View view) {
        viewHolder = AnimationFragmentViewHolder.newInstance(view);
        viewHolder.setRootViewClickedListener(() -> {
            playAnimation();
        });
    }

    public static Fragment newInstance() {
        return new AnimationFragment();
    }

    private void playAnimation() {
        val animator = AnimationFragmentAnimationsUtils.Companion.newInstance(viewHolder);
        animator.playAnimation();
    }
}
